import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author Dominik Dafert
 * @version 1.1.3
 * Created on 12.05.2016.
 * Java version
 * Copy the complete class to your project, if you want to use it. Also the imports!
 *
 * How to:
 * Initialize the ConfigManagerAPI with the constructor
 * You can make a static variable with the instance of it
 * And use it everywhere in your code
 *
 * Example:
 * MainClass {
 *      public static ConfigManagerAPI configmanager = null;
 *
 *      Main() {
 *        configmanager = new ConfigManagerAPI("C:\\", "config.txt");
 *
 *        configmanager.Set("dafnik_is", "cool");
 *      }
 * }
 *
 * ExampleClass {
 *      public void ExampleMethod() {
 *          string dafnikis = MainClass.configmanager.getString("dafnik_is");
 *      }
 * }
 */

class ConfigManagerAPI {
    private String fileName = null;
    private String filePath = null;

    // Only use debug for Console Applications
    public static boolean DEBUG = false;
    private String Pre = "[CM]";
    private String Success = " - SuccessFUL - ";
    private String Unsuccessful = " - Unsuccessful - ";

    private static boolean CrashMessageOutPut = false;
    private static String CrashMessage = "Exception Message:\n";

    public ConfigManagerAPI(String filePath, String fileName) {
        this.filePath = filePath;
        this.fileName = fileName;

        if (!FileExists()) CreateConfig();
    }

    //--------------------------Costumize-------------------------------------

    private void onConfigCreate() {
        /**
         * This can also be empty
         * If you want to add something in config when you create it, enter it here
         * only execute when config is being created
         */

    }


    //--------------------------Set-------------------------------------------

    public void Set(String path, String key) {
        // Set String
        // Check if config exists
        if (FileExists()) {
            AddEntryInConfig(path, key);
        } else {
            CreateConfig();
            Set(path, key);
        }
    }

    public void Set(String path, boolean key) {
        // Set boolean
        // Check if config exists
        if (FileExists()) {
            String skey;
            if (key) {
                skey = "true";
            } else {
                skey = "false";
            }

            AddEntryInConfig(path, skey);
        } else {
            CreateConfig();
            Set(path, key);
        }
    }

    public void Set(String path, int key) {
        // Set int
        // Check if file exists
        if (FileExists()) {
            AddEntryInConfig(path, String.valueOf(key));
        } else {
            CreateConfig();
            Set(path, key);
        }
    }

    public void Set(String path, double key) {
        // Set double
        // Check if file exists
        if (FileExists()) {
            AddEntryInConfig(path, String.valueOf(key));
        } else {
            CreateConfig();
            Set(path, key);
        }
    }


    //---------------------------READ--------------------------------------

    public boolean GetBoolean(String path) {
        // Get boolean
        // Check if file exists
        if (FileExists()) {
            if (GetFromConfig(path) != null) {
                return GetFromConfig(path).equalsIgnoreCase("true");
            } else {
                if (DEBUG) System.out.println(Pre + Unsuccessful + "getBool | Path: " + path + " | Autoreturn: false");
                return false;
            }
        } else {
            CreateConfig();
            if (DEBUG) System.out.println(Pre + Unsuccessful + "getBool | Reason: There is no config | Autoreturn: false");
            return false;
        }
    }

    public String GetString(String path) {
        // Get String
        // Check if file exists
        if (FileExists()) {
            return GetFromConfig(path);
        } else {
            CreateConfig();
            if (DEBUG)
                System.out.println(Pre + Unsuccessful + "getString | Reason: There is no config | Autoreturn: null");
            return null;
        }
    }

    public int GetInt(String path) {
        // Get integer
        // Check if file exists
        if (FileExists()) {
            try {
                if (GetFromConfig(path) != null)
                    return Integer.parseInt(GetFromConfig(path));
                else {
                    if (DEBUG)
                        System.out.println(Pre + Unsuccessful + "getInt | Path: " + path + " | Autoreturn: " + Integer.MIN_VALUE);
                    return Integer.MIN_VALUE;
                }
            } catch (Exception ex) {
                if (DEBUG)
                    System.out.println(Pre + Unsuccessful + "getInt | Path: " + path + " | Autoreturn: " + Integer.MIN_VALUE);
                return Integer.MIN_VALUE;
            }
        } else {
            CreateConfig();
            if (DEBUG)
                System.out.println(Pre + Unsuccessful + "getInt | Reason: There is no config | Autoreturn: " + Integer.MIN_VALUE);
            return Integer.MIN_VALUE;
        }
    }

    public double GetDouble(String path) {
        // Get double
        // Check if file exists
        if (FileExists()) {
            try {
                return Double.parseDouble(GetFromConfig(path));
            } catch (Exception ex) {
                if (DEBUG)
                    System.out.println(Pre + Unsuccessful + "GetDouble | Path: " + path + " | Autoreturn: " + Double.MIN_VALUE);
                return Double.MIN_VALUE;
            }
        } else {
            CreateConfig();
            if (DEBUG)
                System.out.println(Pre + Unsuccessful + "GetDouble | Reason: There is no config | Autoreturn: " + Double.MIN_VALUE);
            return Double.MIN_VALUE;
        }
    }


    //--------------------------Remove-------------------------------------

    public void RemoveEntry(String path) {
        // Remove entry from file
        // Check if file exists
        if (FileExists()) {
            try {
                // Line to remove
                String linetoremove = null;

                // Read file to list lines
                List<String> lines = ReadFile();

                // Check if lines contains path
                for (String line : lines) {
                    if (line.contains(path)) {
                        linetoremove = line;
                        break;
                    }
                }

                // If linetoremove changed, remove it from list
                if (linetoremove != null) lines.remove(linetoremove);

                // Write list lines to file
                WriteFile(lines);
            } catch (Exception ex) {
                if (DEBUG) System.out.println(Pre + Unsuccessful + "RemoveEntry | Path: " + path);
                if (CrashMessageOutPut) System.out.println(CrashMessage + ex.getMessage());
            }
        } else {
            CreateConfig();
            RemoveEntry(path);
        }
    }

    public void DeleteConfig(String confirm) {
        // Delete the file
        // Check if file exists
        if (FileExists()) {
            // Check if confirm
            if (confirm.equalsIgnoreCase("I really want to delete the config! There is no way back!")) {
                if(!(new File(filePath + fileName).delete() && new File(filePath).delete())) {
                    if (DEBUG) System.out.println(Pre + Unsuccessful + "DeleteConfig | Reason: Unknown");
                }
            } else {
                if (DEBUG) System.out.println(Pre + Unsuccessful + "DeleteConfig | Confirm text: \"I really want to delete the config! There is no way back!\"");
            }
        } else {
            if (DEBUG) System.out.println(Pre + Unsuccessful + "DeleteConfig | Reason: File doesn't exists");
        }
    }


    //--------------------------Core---------------------------------------

    private void AddEntryInConfig(String path, String key) {
        // Add entry in config
        // Check if file exist
        if (FileExists()) {
            try {
                // Entry which is saved to the file
                String newline = path + ":" + key;

                // Read file
                List<String> lines = ReadFile();

                // If file contains new line, remove that
                String todeleteline = null;

                // If lines already exist, delete the old line
                for (String line : lines) {
                    //line contains path
                    if (line.contains(path)) {
                        //Set to delete line
                        todeleteline = line;
                        break;
                    }
                }

                // If todeleteline changed, remove it from list lines
                if (todeleteline != null) lines.remove(todeleteline);

                // Add new line to list
                lines.add(newline);

                // Write to file new list
                WriteFile(lines);

                if (DEBUG) System.out.println(Pre + Success + "addInConfig | Path: " + path + " | Key: " + key);
            } catch (Exception ex) {
                if (DEBUG) System.out.println(Pre + Unsuccessful + "addInConfig | Path: " + path + " | Key: " + key);
                if (CrashMessageOutPut) System.out.println(CrashMessage + ex.getMessage());
            }
        } else {
            CreateConfig();
            AddEntryInConfig(path, key);
        }
    }

    private String GetFromConfig(String path) {
        // Get from config
        // Check if file exists
        if (FileExists()) {
            // Line which is searched
            String searchedline = null;

            try {
                // Read file to lines
                List<String> lines = ReadFile();

                for (String line : lines) {
                    // If line contains path
                    if (line.contains(path)) {
                        // Set searched line
                        searchedline = line.substring(line.indexOf(":") + 1);
                        break;
                    }
                }
            } catch (Exception ex) {
                if (DEBUG) System.out.println(Pre + Unsuccessful + "GetFromConfig | Path: " + path + " | Autoreturn: null");
                if (CrashMessageOutPut) System.out.println(CrashMessage + ex.getMessage());
            }

            return searchedline;
        } else {
            CreateConfig();
            GetFromConfig(path);
            return null;
        }
    }

    private boolean FileExists() {
        // Check if file exists
        return (new File(filePath).exists() && new File(filePath + fileName).exists());
    }

    private void CreateConfig() {
        // Create file
        try {
            if(!(new File(filePath).mkdir() && new File(filePath + fileName).createNewFile())) {
                if (DEBUG) System.out.println(Pre + Unsuccessful + "CreateConfig | Reason: Can not create Config");
                return;
            }

            // Set some values for Config create
            onConfigCreate();
        } catch (Exception ex) {
            if (DEBUG) System.out.println(Pre + Unsuccessful + "CreateConfig | Reason: " + ex.getMessage());
            if (CrashMessageOutPut) System.out.println(CrashMessage + ex.getMessage());
            return;
        }

        if (DEBUG) System.out.println(Pre + Success + "CreateConfig");
    }

    private List<String> ReadFile() {
        // Read the file

        List<String> lines = new ArrayList<>();

        // Read all lines to list from file
        try {
            Scanner sc = new Scanner(new File(filePath + fileName));
            while (sc.hasNextLine()) {
                lines.add(sc.nextLine());
            }
            sc.close();
        } catch (Exception ex) {
            if (DEBUG) System.out.println(Pre + Unsuccessful + "ReadFile | Reason: " + ex.getMessage());
            if (CrashMessageOutPut) System.out.println(CrashMessage + ex.getMessage());
        }

        // Return list
        return lines;
    }

    private void WriteFile(List<String> lines) {
        // Write list to file
        try {
            FileWriter fileWriter = new FileWriter(new File(filePath + fileName));
            PrintWriter writer = new PrintWriter(fileWriter);

            for (String line : lines) {
                writer.println(line);
            }

            writer.close();

        } catch (Exception ex) {
            if (DEBUG) System.out.println(Pre + Unsuccessful + "WriteFile | Reason: " + ex.getMessage());
            if (CrashMessageOutPut) System.out.println(CrashMessage + ex.getMessage());
        }
    }
}

